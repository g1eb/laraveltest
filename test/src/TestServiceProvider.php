<?php

namespace Packages\Laraveltest;

use Illuminate\Support\ServiceProvider;

class TestServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
	    include __DIR__.'/routes.php';
	    $this->app->make('Packages\Laraveltest\TestController');
    }
}
