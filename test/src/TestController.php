<?php
/**
 * Created by PhpStorm.
 * User: gleb
 * Date: 8/19/16
 * Time: 13:06
 */

namespace Packages\Laraveltest;

use App\Http\Controllers\Controller;

class TestController extends Controller
{
	public function index()
	{
		echo "Hello, World!";
	}
}